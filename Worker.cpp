/*
 * Worker.cpp
 *
 * Created: 30.03.2020 21:52:24
 *  Author: gsaod
 */

#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <string.h>

#include "Worker.h"
#include "conf_mem.h"

Worker::
Worker(SevSegDynamic* pdisplay, DS1302* prtc, DS18B20* pts, Timer_1* pflowCounter) :
    _pDisplay(pdisplay),
    _prtc(prtc),
    _pTermoSensor(pts),
    _pflowCounter(pflowCounter),
    _lampCurrentTemp(0),
    _lampMaxTemp(0),
    _lastDispValue(dRevision),
    _currentFlowCount(0),
    _lampPowerState(false),
    _lampSecsCounter(0),
    _lampSecsCheckCounter(0),
    _nigthStart(0),
    _nigthStop(0),
    _nigthEnable(0),
    _lampEnable(1),
    _alarmFlag(false),
    _lampFailFlag(false),
    _lampTempChange(false),
    _dayWaterCounter(0),
    _flowCalibrateValue(DEF_EEPROM_FLOW_CAL),
    _dayMembraneCounter(0)
{
    // Снять защиту записи часов
    prtc->writeProtect(false);

    // Считать калибровочное значение литра жидкости
    _flowCalibrateValue = eeprom_read_word((uint16_t*)ADR_EEPROM_FLOW_CAL);

    // Максимальная температура лампы
    _lampMaxTemp = MAX_LAMP_TEMP;

    // Записать калибровочное значение в регистр сравнения счетчика
    _pflowCounter->setCompareValue(_flowCalibrateValue);

    // Считать и инициализировать счетчики
    _filter123.init(ADR_NVRAM_F_123, ADR_EEPROM_F_123_LIMIT, DEF_EEPROM_F_123_LIMIT, _prtc);
    _filterPostCarbone.init(ADR_NVRAM_F_POST, ADR_EEPROM_F_POST_LIMIT, DEF_EEPROM_F_POST_LIMIT, _prtc);
    _filterMembrane.init(ADR_NVRAM_MEMBRANE, ADR_EEPROM_MEMBRANE_LIMIT, DEF_EEPROM_MEMBRANE_LIMIT, _prtc);
    _lampUV.init(ADR_NVRAM_LAMP, ADR_EEPROM_LAMP_LIMIT, DEG_EEPROM_LAMP_LIMIT, _prtc);

    // Считать время ночного режима
    _nigthStart = eeprom_read_byte((uint8_t*)ADR_EEPROM_NIGTH_START);
    _nigthStop = eeprom_read_byte((uint8_t*)ADR_EEPROM_NIGTH_STOP);
    _nigthEnable = eeprom_read_byte((uint8_t*)ADR_EEPROM_NIGTH_ENABLE);

    _lampEnable = eeprom_read_byte((uint8_t*)ADR_EEPROM_LAMP_ENABLE);

    // Считать суточный расход
    _dayWaterCounter = _prtc->readRamWord(ADR_NVRAM_DAY_CNT);

    // Время работа мембраны, дней
    _dayMembraneCounter = _prtc->readRamWord(ADR_NVRAM_DAY_MEMBRANE_CNT);

    // Считать текущий счет
    _currentFlowCount = _prtc->readRamWord(ADR_NVRAM_CURRENT_CNT);
    _pflowCounter->set(_currentFlowCount);
}

Worker::
~Worker()
{

}

void
Worker::
factoryReset()
{
    // Сбросить все счетчики
    _filter123.factoryReset();
    _filterPostCarbone.factoryReset();
    _filterMembrane.factoryReset();
    _lampUV.factoryReset();

    // Сбросить калибровочное значение турбины
    eeprom_write_word((uint16_t*)ADR_EEPROM_FLOW_CAL, DEF_EEPROM_FLOW_CAL);
    _pflowCounter->setCompareValue(DEF_EEPROM_FLOW_CAL);

    // Сбросить параметры ночного режима
    eeprom_write_byte((uint8_t*)ADR_EEPROM_NIGTH_START, DEF_EEPROM_NIGTH_START);
    _nigthStart = DEF_EEPROM_NIGTH_START;

    eeprom_write_byte((uint8_t*)ADR_EEPROM_NIGTH_STOP, DEF_EEPROM_NIGTH_STOP);
    _nigthStop = DEF_EEPROM_NIGTH_STOP;

    eeprom_write_byte((uint8_t*)ADR_EEPROM_NIGTH_ENABLE, DEF_EEPROM_NIGTH_ENABLE);
    _nigthEnable = DEF_EEPROM_NIGTH_ENABLE;

    eeprom_write_byte((uint8_t*)ADR_EEPROM_LAMP_ENABLE, DEF_EEPROM_LAMP_ENABLE);
    _lampEnable = DEF_EEPROM_LAMP_ENABLE;

    // Сбросить суточный и текущий счетчики
    _prtc->writeRamWord(ADR_NVRAM_DAY_CNT, 0);
    _dayWaterCounter = 0;

    _prtc->writeRamWord(ADR_NVRAM_CURRENT_CNT, 0);
    _currentFlowCount = 0;

    // Сбросить число дней работы мембраны
    _prtc->writeRamWord(ADR_NVRAM_DAY_MEMBRANE_CNT, 0);
    _dayMembraneCounter = 0;
}

void
Worker::
displayRefresh()
{
    static uint8_t lampLedFlashCounter = 0;

    _pDisplay->refresh();

    // Если лампа вышла из строя - быстро моргать
    if (_lampFailFlag && _lampEnable)
    {
        if (lampLedFlashCounter++ > 40)
        {
            lampLedFlashCounter = 0;
            DigitalLevelToggle(LAMP_LED);
        }
    }
}

void
Worker::
resetRTC()
{
    // Сбросить бит защиты записи
    _prtc->writeProtect(0);

    // Обнулить секунды
    _prtc->writeRegister(0, 0);

    // Выставить нулевое время
    _prtc->time(_realTime);
}

void
Worker::
getTemp()
{
    int16_t temp = 0;

    // Выключить дисплей
    _pDisplay->turnOff();

    // Произвести измерение температуры
    cli();
    temp = _pTermoSensor->ReadTemp();
    sei();

    // Если температура невалидна - оборван датчик, выйти
    if (temp == 0)
        return;

    // Если температура изменилась - выставить флаг
    if (temp != _lampCurrentTemp)
        _lampTempChange = true;

    // Присвоить температуру
    _lampCurrentTemp = temp;

    if (!_lampEnable)
        return;

    // Выключить лампу, если температура превышена или равна нулю
    if ((_lampCurrentTemp >= _lampMaxTemp) || (_lampCurrentTemp == 0))
    {
        DigitalWrite(LAMP_PWR, Low);

        _lampPowerState = false;
    }
}

void
Worker::
getRTC()
{
   _pDisplay->turnOff();
   _realTime = _prtc->time();

    // Выключить лампу, в момент начала ночного режима
    if (_lampEnable && _lampPowerState && _nigthEnable && (_realTime._hr == _nigthStart) && (_realTime._min == 0))
    {
        DigitalWrite(LAMP_PWR, Low);

        _lampPowerState = false;

        // Сбросить проверочный счетчик
        _lampSecsCheckCounter = 0;
    }

    // В полночь
    if ((_realTime._hr == 0) && (_realTime._min == 0))
    {
        // Cбросить суточный счет жидкости
        _dayWaterCounter = 0;
        _prtc->writeRamWord(ADR_NVRAM_DAY_CNT, 0);

        // Увеличить дни работы мембраны
        _dayMembraneCounter++;
        _prtc->writeRamWord(ADR_NVRAM_DAY_MEMBRANE_CNT, _dayMembraneCounter);

        // Считать время работы мембраны до сброса
        uint16_t dayMembraneCounterBefore = eeprom_read_word((uint16_t*)ADR_EEPROM_DAY_BEFORE);

        // Если что то записано и пришло время обсчета (10 дней)
        if ((dayMembraneCounterBefore != 0xFFFF) && (_dayMembraneCounter > 10))
        {
            recalculationReminder(dayMembraneCounterBefore);
        }

    }

    // Сохранить счет в NVRAM
    _prtc->writeRamWord(ADR_NVRAM_CURRENT_CNT, _currentFlowCount);
}

void
Worker::
recalculationReminder(uint16_t before)
{
    // Вычислить средний расход в литрах за период после запуска
    uint16_t dayWaterCounter = _filter123.getAmount() / _dayMembraneCounter;

    // Всего дней отработано
    uint16_t daysSummary = before + _dayMembraneCounter;

    // Общий расход по фильтрам
    uint16_t filterSummary = dayWaterCounter * daysSummary;

    // Общий расход по лампе - предполагается круглосуточная работа
    uint16_t lampSummary = 24 * daysSummary;

    // По мембране и лампе установить фактически отработанные значения
    _filterMembrane.setAmount(filterSummary);
    _lampUV.setAmount(lampSummary);

    // Получить лимиты
    uint16_t filter123Limit = _filter123.getLimit();
    uint16_t filterPostCarboneLimit = _filterPostCarbone.getLimit();

    // Если суммарный расход превышает лимит
    if (filterSummary > filter123Limit )
    {
        // Предполагается, что фильтры вовремя менялись, установить остаток
        _filter123.setAmount(filterSummary % filter123Limit);
    }
    else
    {
        // Иначе записать как есть
        _filter123.setAmount(filterSummary);
    }

    if (filterSummary > filterPostCarboneLimit)
    {
        _filterPostCarbone.setAmount(filterSummary % filterPostCarboneLimit);
    }
    else
    {
        _filterPostCarbone.setAmount(filterSummary);
    }

    // Обновить суммарное время работы
    _prtc->writeRamWord(ADR_NVRAM_DAY_MEMBRANE_CNT, daysSummary);
    _dayMembraneCounter = daysSummary;

    // Затереть время работы до перерасчета
    cli();
    eeprom_write_word((uint16_t*)ADR_EEPROM_DAY_BEFORE, 0xFFFF);
    sei();
}

void
Worker::
updateCounters()
{
    uint16_t tmp =  _pflowCounter->getCount();

    // Если счетное значение изменилось
    if (_currentFlowCount != tmp)
    {
        // Включить светодиод потока воды
    	DigitalWrite(FLOW_LED, High);

        // Включить лампу, если она выключена
        if (!_lampPowerState && _lampEnable)
        {
            // Обеспечить температурный гистерезис
            if (_lampCurrentTemp < _lampMaxTemp - TEMP_HYSTERESIS)
            {
                DigitalWrite(LAMP_PWR, High);
                _lampPowerState = true;

                // Сбросить проверочный счетчик
                _lampSecsCheckCounter = 0;

                // Сбросить флаг изменения температуры
                _lampTempChange = false;

                _lampFailFlag = false;
            }
            // Лампа не будет включена, если хотя бы немного не остыла
            // Будет подаваться беспрерывный звуковой сигнал
            else
            {
                DigitalWrite(BUZER, Low);
            }
        }

        // Если лампа вышла из строя - подавать звуковой сигнал во время расхода воды
        if (_lampFailFlag && _lampEnable)
            DigitalWrite(BUZER, Low);

        // Если остаток по любому из счетчиков меньше предела - выставить флаг звукового предупреждения
        if ((_filter123.getRemainder() < BEEP_LIMIT) ||
           (_filterPostCarbone.getRemainder() < BEEP_LIMIT) ||
           (_filterMembrane.getRemainder() < BEEP_LIMIT) ||
           (_lampUV.getRemainder() < BEEP_LIMIT))
        {
            _alarmFlag = true;
        }
    }
    // Поток остановился
    else
    {
        // Погасить светодиод потока
        DigitalWrite(FLOW_LED, Low);

        // Если был выставлен флаг звукового предпреждения
        if (_alarmFlag)
        {
            // Включить звук
            _alarmFlag = false;
            DigitalWrite(BUZER, Low);
        }
        else
        {
            // Выключить звук
            DigitalWrite(BUZER, High);
        }
    }

    // Сохранить текущее счетное значение
    _currentFlowCount = tmp;

    if (!_lampEnable)
        return;

    // Если состояние лампы включено
    if (_lampPowerState)
    {
        // Включить сетодиод лампы
        if (!_lampFailFlag)
           DigitalWrite(LAMP_LED, High);

        // Инкриментировать время работы
        if(++_lampSecsCounter >= 3600)
        {
            _lampSecsCounter = 0;
            _lampUV.nextAmountValue();
        }

        // Если пришло время проверки работоспособности лампы
        if(_lampSecsCheckCounter++ == LAMP_CHECK_TIMEOUT)
        {
            // Следующая проверка будет повторена через такой же промежуток времени
            _lampSecsCheckCounter = 0;

            // Если температура не изменилась
            if (!_lampTempChange)
                _lampFailFlag = true;
            else
            {
                // Проверяем дальше
                _lampFailFlag = false;
                _lampTempChange = false;
            }
        }
    }
    else if (!_lampFailFlag)
    {
        DigitalWrite(LAMP_LED, Low);
    }
}

void
Worker::
displayNextValue()
{
    char buffer[6];
    memset(buffer, 0, 6);

    uint16_t tenth;

    static uint8_t revisionViewCounter = 0;

    switch(_lastDispValue)
    {

        case dRevision :
            revisionViewCounter++;
            strcpy(buffer, REVISION);
        break;

        case dTime :
            buffer[0] = (_realTime._hr / 10) + 0x30;
            buffer[1] = (_realTime._hr % 10) + 0x30;
            buffer[2] = '.';
            buffer[3] = (_realTime._min / 10) + 0x30;
            buffer[4] = (_realTime._min % 10) + 0x30;
        break;

        case dDayWaterCounter :
            // Гашение незначащего нуля
            if (_dayWaterCounter > 9 )
                buffer[0] = (_dayWaterCounter / 10) + 0x30;
            else
                buffer[0] = ' ';

            buffer[1] = (_dayWaterCounter % 10) + 0x30;
            buffer[2] = '.';

            // Десятая доля литра
            tenth =  _currentFlowCount * 10 / _flowCalibrateValue;
            if (tenth > 9)
                tenth = 0;
            buffer[3] = (tenth) + 0x30;

            buffer[4] = 'L';
        break;

        case dTemp :
            buffer[0] = (_lampCurrentTemp / 10) + 0x30;
            buffer[1] = (_lampCurrentTemp % 10) + 0x30;
            buffer[2] = 'c';
            buffer[3] = ' ';
        break;

        case dF123 :
            buffer[0] = 'F';
            buffer[1] = '-';
            buffer[2] = (_filter123.getRemainder() / 10) + 0x30;
            buffer[3] = (_filter123.getRemainder()  % 10) + 0x30;
        break;

        case dFpost :
            buffer[0] = 'P';
            buffer[1] = '-';
            buffer[2] = (_filterPostCarbone.getRemainder() / 10) + 0x30;
            buffer[3] = (_filterPostCarbone.getRemainder() % 10) + 0x30;
        break;

        case dMembrane :
            buffer[0] = 'H';
            buffer[1] = '-';
            buffer[2] = (_filterMembrane.getRemainder() / 10) + 0x30;
            buffer[3] = (_filterMembrane.getRemainder() % 10) + 0x30;
        break;

        case dLamp :
            if (!_lampFailFlag)
            {
                buffer[0] = 'L';
                buffer[1] = '-';
                buffer[2] = (_lampUV.getRemainder() / 10) + 0x30;
                buffer[3] = (_lampUV.getRemainder() % 10) + 0x30;
            }
            else
            {
                buffer[0] = 'F';
                buffer[1] = 'A';
                buffer[2] = 'I';
                buffer[3] = 'L';
            }
        break;

        default: break;
    }

    _pDisplay->setText(buffer);

    if (revisionViewCounter < 5)
        return;

    if(++_lastDispValue >= dLastItem)
       _lastDispValue = dTime;
};

void
Worker::
incAmounts()
{
    _pDisplay->turnOff();

    _filter123.nextAmountValue();
    _filterPostCarbone.nextAmountValue();
    _filterMembrane.nextAmountValue();

    _dayWaterCounter++;

    if (_dayWaterCounter > 99)
        _dayWaterCounter = 99;

    _prtc->writeRamWord(ADR_NVRAM_DAY_CNT, _dayWaterCounter);
}

void
Worker::
offDisplay()
{
    _pDisplay->turnOff();
}

void
Worker::
updateFromNvRam()
{
    _filter123.updateFromNvRam();
    _filterPostCarbone.updateFromNvRam();
    _filterMembrane.updateFromNvRam();
    _lampUV.updateFromNvRam();

    _dayMembraneCounter = _prtc->readRamWord(ADR_NVRAM_DAY_MEMBRANE_CNT);

    // Считать время ночного режима
    _nigthStart = eeprom_read_byte((uint8_t*)ADR_EEPROM_NIGTH_START);
    _nigthStop = eeprom_read_byte((uint8_t*)ADR_EEPROM_NIGTH_STOP);
    _nigthEnable = eeprom_read_byte((uint8_t*)ADR_EEPROM_NIGTH_ENABLE);
    _lampEnable = eeprom_read_byte((uint8_t*)ADR_EEPROM_LAMP_ENABLE);
}