/*
 * Timer_0.h
 *
 * Created: 28.03.2020 12:28:44
 *  Author: gsaod
 */


#ifndef TIMER_0_H_
#define TIMER_0_H_

#include "iTimer.h"

class Timer_0 : public iTimer
{
    public:

    Timer_0(T_PRESCALER prescale, T_MODE mode);

    void reset();
    uint16_t getCount();

    void setInterrupt(T_INTERRUPT interrupt);
    void set(const uint16_t count);
    void setCompareValue(const uint16_t count);
};

#endif /* TIMER_0_H_ */