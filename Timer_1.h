/*
 * Timer_1.h
 *
 * Created: 31.03.2020 21:06:28
 *  Author: gsaod
 */


#ifndef TIMER_1_H_
#define TIMER_1_H_

#include "iTimer.h"

class Timer_1 : public iTimer
{
    public:

    Timer_1(T_PRESCALER prescale, T_MODE mode);

    void reset();
    uint16_t getCount();

    void setInterrupt(T_INTERRUPT interrupt);
    void set(const uint16_t count);
    void setCompareValue(const uint16_t count);
};




#endif /* TIMER_1_H_ */