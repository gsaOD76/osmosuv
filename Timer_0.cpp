/*
 * Timer_0.cpp
 *
 * Created: 28.03.2020 12:45:56
 *  Author: gsaod
 */

#include "Timer_0.h"

Timer_0::
Timer_0(T_PRESCALER prescale, T_MODE mode) : iTimer(prescale, mode)
{
	switch (mode)
	{
    	case NORMAL :
    	    BitClear(TCCR0, WGM00);
    	    BitClear(TCCR0, WGM01);
    	break;

    	case CTC :
    	    BitClear(TCCR0, WGM00);
    	    BitSet(TCCR0, WGM01);
    	break;

        case PWM_PC :
        case PWM_FC :
            BitSet(TCCR0, WGM00);
            BitClear(TCCR0, WGM01);
        break;

    	case PWM_F :
    	    BitSet(TCCR0, WGM00);
    	    BitSet(TCCR0, WGM01);
    	break;

    	default : break;
	}

    switch (prescale)
    {
        TCCR0 &= 0b11111000;

        case PRESCALER_OFF :
        break;

        case PRESCALER_1 :
            TCCR0 |= 0b00000001;
        break;

        case PRESCALER_8 :
            TCCR0 |= 0b00000010;
        break;

        case PRESCALER_64 :
            TCCR0 |= 0b00000011;
        break;

        case PRESCALER_256 :
            TCCR0 |= 0b00000100;
        break;

        case PRESCALER_1024 :
            TCCR0 |= 0b00000101;
        break;

        case PRESCALER_EXT_FALLING :
            TCCR0 |= 0b00000110;
        break;

        case PRESCALER_EXT_RISING :
            TCCR0 |= 0b00000111;
        break;

        default: break;
    }
}


void
Timer_0::
reset()
{
    TCNT0 = 0;
}


uint16_t
Timer_0::
getCount()
{
    return TCNT0;
}

void
Timer_0::
setInterrupt(T_INTERRUPT interrupt)
{
    switch (interrupt)
    {
        case NONE_I :
        break;

        case OVERFLOW :
            BitClear(TIFR, TOV0);
            BitSet(TIMSK, TOIE0);
        break;

        case COMPARE :
            BitClear(TIFR, OCF0);
            BitSet(TIMSK, OCIE0);
        break;

        default: break;
    }
}

void
Timer_0::
set(const uint16_t count)
{
    TCNT0 = count;
}


void
Timer_0::
setCompareValue(const uint16_t count)
{
    OCR0 = count;
}
