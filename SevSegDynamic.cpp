/*
 * SevSegDynamic.cpp
 *
 * Created: 26.03.2020 18:38:28
 *  Author: gsaod76@gmail.com
 */

#include "SevSegDynamic.h"


SevSegDynamic::
SevSegDynamic(uint8_t pinA, uint8_t pinB, uint8_t pinC, uint8_t pinD, uint8_t pinE, uint8_t pinF, uint8_t pinG, uint8_t pinDP,
	uint8_t pinD1, uint8_t pinD2, uint8_t pinD3, uint8_t pinD4,
    uint8_t digitCount, CommonMode mode) :
    _currentDigit(0),
    _currentStrPos(0)
{
    // Назначить сегменты
	_seg[0] = pinA;
	_seg[1] = pinB;
	_seg[2] = pinC;
	_seg[3] = pinD;
	_seg[4] = pinE;
	_seg[5] = pinF;
	_seg[6] = pinG;
	_seg[7] = pinDP;

    // Назначить цифры
	_digits[0] = pinD1;
	_digits[1] = pinD2;
	_digits[2] = pinD3;
	_digits[3] = pinD4;

	_commonMode = mode;

    // В зависимости от общего элемента индикатора установить уровни управления
    if(mode == COMMON_ANODE)
    {
        _DigitOn = Low;
        _DigitOff = High;
        _SegOn = Low;
        _SegOff = High;
    }
    else
    {
        _DigitOn = High;
        _DigitOff = Low;
        _SegOn = High;
        _SegOff = Low;
    }

    // Проверить максимальное число цифр дисплея
    if (digitCount > MAX_DIGIT)
        digitCount = MAX_DIGIT;

    _digitCount = digitCount;

    // Инициализация портов сегментов
    for (uint8_t i = 0; i < 8; i++)
    {
        // Установить выводы сегмента как выходы
        BitSet(SEG_DDR, _seg[i]);

        // Индикатор должен быть выключен!
       DigitalPortWrite(SEG_PORT, _seg[i], _SegOff);
    }

    // Инициализация портов цифр
    for (uint8_t i = 0; i < 4; i++)
    {
        // Установить выводы цифр как выходы
        BitSet(DIG_DDR, _digits[i]);

        // Индикатор должен быть выключен!
        DigitalPortWrite(DIG_PORT, _digits[i], _DigitOff);
    }
}

void
SevSegDynamic::
turnOff()
{
   for (uint8_t i = 0; i < _digitCount; i++)
       DigitalPortWrite(DIG_PORT, _digits[i], _DigitOff);

    DigitalPortWrite(SEG_PORT, _seg[0], _SegOff);
    DigitalPortWrite(SEG_PORT, _seg[1], _SegOff);
    DigitalPortWrite(SEG_PORT, _seg[2], _SegOff);
    DigitalPortWrite(SEG_PORT, _seg[3], _SegOff);
    DigitalPortWrite(SEG_PORT, _seg[4], _SegOff);
    DigitalPortWrite(SEG_PORT, _seg[5], _SegOff);
    DigitalPortWrite(SEG_PORT, _seg[6], _SegOff);
    DigitalPortWrite(SEG_PORT, _seg[7], _SegOff);
}

void
SevSegDynamic::
setText(char* text)
{
    uint8_t i = 0;

    while (*text)
    {
        _displayText[i++]  = *text++;
    }
}

void
SevSegDynamic::
refresh()
{
    // Символ, который будет отображаться для этой цифры
    unsigned char characterToDisplay = _displayText[_currentStrPos];

    // Если сейчас символ точка - переход на следующий
    if (characterToDisplay == '.')
    {
        _currentStrPos++;
        characterToDisplay = _displayText[_currentStrPos];
    }

    // Погасить все цифры  и сегменты
    turnOff();

    // Включить очередную цифру
    DigitalPortWrite(DIG_PORT, _digits[_currentDigit], _DigitOn);

    // Если выбран 7 бит, то включается режим контроля по сегментам
    if (characterToDisplay & 0x80)
    {
        // Каждый бит включает свой сегмент
        if (characterToDisplay & 0x01) DigitalPortWrite(SEG_PORT, _seg[0], _SegOn);
        if (characterToDisplay & 0x02) DigitalPortWrite(SEG_PORT, _seg[1], _SegOn);
        if (characterToDisplay & 0x04) DigitalPortWrite(SEG_PORT, _seg[2], _SegOn);
        if (characterToDisplay & 0x08) DigitalPortWrite(SEG_PORT, _seg[3], _SegOn);
        if (characterToDisplay & 0x10) DigitalPortWrite(SEG_PORT, _seg[4], _SegOn);
        if (characterToDisplay & 0x20) DigitalPortWrite(SEG_PORT, _seg[5], _SegOn);
        if (characterToDisplay & 0x40) DigitalPortWrite(SEG_PORT, _seg[6], _SegOn);
    }
    else
    {
        // Выводится символ, если он есть
        const uint8_t chr = pgm_read_byte(&characterArray[characterToDisplay]);
        if (chr & (1<<6)) DigitalPortWrite(SEG_PORT, _seg[0], _SegOn);
        if (chr & (1<<5)) DigitalPortWrite(SEG_PORT, _seg[1], _SegOn);
        if (chr & (1<<4)) DigitalPortWrite(SEG_PORT, _seg[2], _SegOn);
        if (chr & (1<<3)) DigitalPortWrite(SEG_PORT, _seg[3], _SegOn);
        if (chr & (1<<2)) DigitalPortWrite(SEG_PORT, _seg[4], _SegOn);
        if (chr & (1<<1)) DigitalPortWrite(SEG_PORT, _seg[5], _SegOn);
        if (chr & (1<<0)) DigitalPortWrite(SEG_PORT, _seg[6], _SegOn);
    }

    // Если следующий символ - точка
    if ( _displayText[_currentStrPos + 1] == '.')
    {
        // Включить сегмент точки
        DigitalPortWrite(SEG_PORT, _seg[7], _SegOn);
    }

    // Следующая цифра
    _currentDigit++;

    // Следующий символ
    _currentStrPos++;

    // Заново, если достигнута граница
    if (_currentDigit > (_digitCount - 1))
    {
        _currentDigit = 0;
        _currentStrPos = 0;
    }
}



