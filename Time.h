/*
 * Time.h
 *
 * Created: 29.03.2020 12:11:15
 *  Author: gsaod
 */


#ifndef TIME_H_
#define TIME_H_

#include <stdint.h>
#include <stddef.h>

class Time
{
    public:
    enum Day
    {
        kSunday    = 1,
        kMonday    = 2,
        kTuesday   = 3,
        kWednesday = 4,
        kThursday  = 5,
        kFriday    = 6,
        kSaturday  = 7
    };

    Time(const uint16_t yr, const uint8_t mon, const uint8_t date,
         const uint8_t hr, const uint8_t min, const uint8_t sec,
         const Day day) :
    _yr(yr),
    _mon(mon),
    _date(date),
    _hr(hr),
    _min(min),
    _sec(sec),
    _day(day)
    {

    }

    Time() :
    _yr(2000),
    _mon(1),
    _date(1),
    _hr(0),
    _min(0),
    _sec(0),
    _day(Time::kSunday)
    {

    }

    uint16_t _yr;
    uint8_t _mon;
    uint8_t _date;
    uint8_t _hr;
    uint8_t _min;
    uint8_t _sec;
    Day _day;
};



#endif /* TIME_H_ */