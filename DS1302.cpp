/*
 * DS1302.cpp
 *
 * Created: 29.03.2020 12:32:18
 *  Author: gsaod
 */

#include <util/delay.h>

#include "ds1302.h"

namespace
{

    enum Register
    {
        kSecondReg       = 0,
        kMinuteReg       = 1,
        kHourReg         = 2,
        kDateReg         = 3,
        kMonthReg        = 4,
        kDayReg          = 5,
        kYearReg         = 6,
        kWriteProtectReg = 7,

        // Количество регистров RAM памяти
        kRamAddress0     = 32
    };

    enum Command
    {
        kClockBurstRead  = 0xBF,
        kClockBurstWrite = 0xBE,
        kRamBurstRead    = 0xFF,
        kRamBurstWrite   = 0xFE
    };

    // Создание и уничтожение SPI сессии
    class SPISession
    {
        public:

        SPISession(const int ce_pin, const int io_pin, const int sclk_pin) :
        _ce_pin(ce_pin),
        _io_pin(io_pin),
        _sclk_pin(sclk_pin)
        {
            DigitalPortWrite(DS1302_PORT, _sclk_pin, Low);
            DigitalPortWrite(DS1302_PORT, _ce_pin, High);
            _delay_ms(4);
        }

        ~SPISession()
        {
            DigitalPortWrite(DS1302_PORT, _ce_pin, Low);
            _delay_ms(4);
        }

        private:

        const int _ce_pin;
        const int _io_pin;
        const int _sclk_pin;
    };

    // Преобразование BCD код в десятичный
    uint8_t bcdToDec(const uint8_t bcd)
    {
        return (10 * ((bcd & 0xF0) >> 4) + (bcd & 0x0F));
    }

    // Преобразование десятичного кода в бинарный
    uint8_t decToBcd(const uint8_t dec)
    {
        const uint8_t tens = dec / 10;
        const uint8_t ones = dec % 10;
        return (tens << 4) | ones;
    }

    // Вернуть часы в 24 часовом формате
    uint8_t hourFromRegisterValue(const uint8_t value)
    {
        uint8_t adj;

        if (value & 128)  // 12-часовой
            adj = 12 * ((value & 32) >> 5);
        else           // 24-часовой
            adj = 10 * ((value & (32 + 16)) >> 4);
        return (value & 15) + adj;
    }

}  // namespace

DS1302::
DS1302(const uint8_t ce_pin, const uint8_t io_pin, const uint8_t sclk_pin) :
    _ce_pin(ce_pin),
    _io_pin(io_pin),
    _sclk_pin(sclk_pin)
{
    // Выборка
    DigitalPortWrite(DS1302_PORT, _ce_pin, Low);
    DigitalPortWrite(DS1302_DDR, _ce_pin, Output);

    // Данные
    DigitalPortWrite(DS1302_DDR, _io_pin, Low);
    DigitalPortWrite(DS1302_DDR, _io_pin, Output);

    // Синхронизация
    DigitalPortWrite(DS1302_PORT, _sclk_pin, Low);
    DigitalPortWrite(DS1302_DDR, _sclk_pin, Output);
}

void
DS1302::
writeOut(const uint8_t value, bool readAfter)
{
    DigitalPortWrite(DS1302_DDR, _io_pin, Output);

    for (int i = 0; i < 8; ++i)
    {
        DigitalPortWrite(DS1302_PORT, _io_pin, (value >> i) & 1);
        _delay_ms(1);

        DigitalPortWrite(DS1302_PORT, _sclk_pin, High);
         _delay_ms(1);

        if (readAfter && i == 7)
        {
            DigitalPortWrite(DS1302_DDR, _io_pin, Input);
        }
        else
        {
            DigitalPortWrite(DS1302_PORT, _sclk_pin, Low);
            _delay_ms(1);
        }
    }
}

uint8_t
DS1302::
readIn()
{
    uint8_t input_value = 0;
    uint8_t bit = 0;

    DigitalPortWrite(DS1302_DDR, _io_pin, Input);

    for (int i = 0; i < 8; ++i)
    {
        DigitalPortWrite(DS1302_PORT, _sclk_pin, High);
        _delay_ms(1);

        DigitalPortWrite(DS1302_PORT, _sclk_pin, Low);
        _delay_ms(1);

        bit = DigitalPortRead(DS1302_PIN, _io_pin);
        input_value |= (bit << i);
    }

    return input_value;
}


uint8_t
DS1302::
readRegister(uint8_t reg)
{
    const SPISession s(_ce_pin, _io_pin, _sclk_pin);

    const uint8_t cmd_byte = (0x81 | (reg << 1));

    writeOut(cmd_byte, true);
    return readIn();
}

void
DS1302::
writeRegister(const uint8_t reg, const uint8_t value)
{
    const SPISession s(_ce_pin, _io_pin, _sclk_pin);

    const uint8_t cmd_byte = (0x80 | (reg << 1));
    writeOut(cmd_byte);
    writeOut(value);
}

void
DS1302::
writeProtect(const bool enable)
{
    writeRegister(kWriteProtectReg, (enable << 7));
}

void
DS1302::
halt(const bool enable)
{
    uint8_t sec = readRegister(kSecondReg);
    sec &= ~(1 << 7);
    sec |= (enable << 7);
    writeRegister(kSecondReg, sec);
}

Time
DS1302::
time()
{
    const SPISession s(_ce_pin, _io_pin, _sclk_pin);

    Time t(2000, 1, 1, 0, 0, 0, Time::kSunday);

    writeOut(kClockBurstRead, true);

    t._sec = bcdToDec(readIn() & 0x7F);
    t._min = bcdToDec(readIn());
    t._hr = hourFromRegisterValue(readIn());
    t._date = bcdToDec(readIn());
    t._mon = bcdToDec(readIn());
    t._day = static_cast<Time::Day>(bcdToDec(readIn()));
    t._yr = 2000 + bcdToDec(readIn());

    return t;
}

void
DS1302::
time(const Time t)
{
    const uint8_t ch_value = readRegister(kSecondReg) & 0x80;

    const SPISession s(_ce_pin, _io_pin, _sclk_pin);

    writeOut(kClockBurstWrite);

    writeOut(ch_value | decToBcd(t._sec));
    writeOut(decToBcd(t._min));
    writeOut(decToBcd(t._hr));
    writeOut(decToBcd(t._date));
    writeOut(decToBcd(t._mon));
    writeOut(decToBcd(static_cast<uint8_t>(t._day)));
    writeOut(decToBcd(t._yr - 2000));

    // Write protection register
    writeOut(0);
}

void
DS1302::
writeRam(const uint8_t address, const uint8_t value)
{
    if (address >= kRamSize)
    {
        return;
    }

    writeRegister(kRamAddress0 + address, value);
}

void
DS1302::
writeRamWord(uint8_t address, uint16_t value)
{
    writeRam(address, static_cast<uint8_t>(value >> 8));
    writeRam(address + 1, static_cast<uint8_t>(value & 0x00FF));
}

uint8_t
DS1302::
readRam(const uint8_t address)
{
    if (address >= kRamSize)
    {
        return 0;
    }

    return readRegister(kRamAddress0 + address);
}

uint16_t
DS1302::
readRamWord(uint8_t address)
{
    uint16_t value =  (readRam(address) << 8) |  readRam(address + 1);
    return value;
}

void
DS1302::
writeRamBulk(const uint8_t* const data, int len)
{
    if (len <= 0)
    {
        return;
    }

    if (len > kRamSize)
    {
        len = kRamSize;
    }

    const SPISession s(_ce_pin, _io_pin, _sclk_pin);

    writeOut(kRamBurstWrite);

    for (int i = 0; i < len; ++i)
    {
        writeOut(data[i]);
    }
}

void DS1302::readRamBulk(uint8_t* const data, int len)
{
    if (len <= 0)
    {
        return;
    }

    if (len > kRamSize)
    {
        len = kRamSize;
    }

    const SPISession s(_ce_pin, _io_pin, _sclk_pin);

    writeOut(kRamBurstRead, true);

    for (int i = 0; i < len; ++i)
    {
        data[i] = readIn();
    }
}