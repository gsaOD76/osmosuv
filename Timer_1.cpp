/*
 * Timer_1.cpp
 *
 * Created: 31.03.2020 21:06:51
 *  Author: gsaod
 */

#include "Timer_1.h"

Timer_1::
Timer_1(T_PRESCALER prescale, T_MODE mode) : iTimer(prescale, mode)
{
    switch (mode)
    {
        case NORMAL :
            BitClear(TCCR1A, WGM10);
            BitClear(TCCR1A, WGM11);
            BitClear(TCCR1B, WGM12);
            BitClear(TCCR1B, WGM13);
        break;

        case CTC :
            BitClear(TCCR1A, WGM10);
            BitClear(TCCR1A, WGM11);
            BitSet(TCCR1B, WGM12);
            BitClear(TCCR1B, WGM13);
        break;

        case PWM_FC :
            BitSet(TCCR1A, WGM10);
            BitClear(TCCR1A, WGM11);
            BitClear(TCCR1B, WGM12);
            BitClear(TCCR1B, WGM13);
        break;

        case PWM_F :
            BitClear(TCCR1A, WGM10);
            BitSet(TCCR1A, WGM11);
            BitSet(TCCR1B, WGM12);
            BitSet(TCCR1B, WGM13);
        break;

        default : break;
    }

    switch (prescale)
    {
        TCCR1B &= 0b11111000;

        case PRESCALER_OFF :
        break;

        case PRESCALER_1 :
         TCCR1B |= 0b00000001;
        break;

        case PRESCALER_8 :
            TCCR1B |= 0b00000010;
        break;

        case PRESCALER_64 :
            TCCR1B |= 0b00000011;
        break;

        case PRESCALER_256 :
            TCCR1B |= 0b00000100;
        break;

        case PRESCALER_1024 :
            TCCR1B |= 0b00000101;
        break;

        case PRESCALER_EXT_FALLING :
            TCCR1B |= 0b00000110;
        break;

        case PRESCALER_EXT_RISING :
        TCCR0 |= 0b00000111;
        break;

        default: break;
    }
}


void
Timer_1::
reset()
{
    TCNT1 = 0;
}


uint16_t
Timer_1::
getCount()
{
    return TCNT1;
}

void
Timer_1::
setInterrupt(T_INTERRUPT interrupt)
{
    switch (interrupt)
    {
        case NONE_I :
        break;

        case OVERFLOW :
            BitClear(TIFR, TOV1);
            BitSet(TIMSK, TOIE1);
        break;

        case COMPARE :
            BitClear(TIFR, OCF1A);
            BitSet(TIMSK, OCIE1A);
        break;

        default: break;
    }
}

void
Timer_1::
set(const uint16_t count)
{
    TCNT1 = count;
}


void
Timer_1::
setCompareValue(const uint16_t count)
{
    OCR1A = count;
}
