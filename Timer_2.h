/*
 * Timer_2.h
 *
 * Created: 28.03.2020 19:46:11
 *  Author: gsaod
 */


#ifndef TIMER_2_H_
#define TIMER_2_H_

#include "iTimer.h"

class Timer_2 : public iTimer
{
    public:

    Timer_2(T_PRESCALER prescale, T_MODE mode);

    void reset();
    uint16_t getCount();

    void setInterrupt(T_INTERRUPT interrupt);
    void set(const uint16_t count);
    void setCompareValue(const uint16_t count);
    void SetAsynMode();

    private:
    bool _asynMode;
};


#endif /* TIMER_2_H_ */