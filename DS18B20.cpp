/*
 * DS18B20.cpp
 *
 * Created: 29.03.2020 20:00:12
 *  Author: gsaod
 */

#include "DS18B20.h"

#define DS_LOW DS18B20_PORT &= ~(1 << DS18B20_1W_PIN); DS18B20_DDR |= (1 << DS18B20_1W_PIN)
#define DS_FREE DS18B20_PORT &= ~(1 << DS18B20_1W_PIN); DS18B20_DDR &= ~(1 << DS18B20_1W_PIN)
#define DS_PULL DS18B20_PORT |= (1 << DS18B20_1W_PIN); DS18B20_DDR |= (1 << DS18B20_1W_PIN)
#define DS_STATE ((DS18B20_PIN & (1 << DS18B20_1W_PIN)) ? 1 : 0)

#define DS_WRITE0 DS_LOW; _delay_us(60); DS_FREE; _delay_us(3)
#define DS_WRITE1 DS_LOW; _delay_us(5); DS_FREE; _delay_us(60)
#define DS_READ DS_LOW; _delay_us(5); DS_FREE; _delay_us(3)

DS18B20::
DS18B20(uint8_t owi_pin) :
    _owi_pin(owi_pin)
{

}

bool
DS18B20::
Reset()
{
    DS_LOW;
    _delay_us(1000);

    DS_FREE;
    _delay_us(70);

    DS_READ;

    if (DS_STATE)
        return false;

    _delay_us(480);
    return true;
}


void
DS18B20::
Write(uint8_t data)
{
    for (int8_t i = 0; i < 8; i++)
    {
        if (data & (1 << i))
        {
            DS_WRITE1;
        }
        else
        {
            DS_WRITE0;
        }
    }
}

uint8_t
DS18B20::
Read()
{
    uint8_t data = 0;

    for (int8_t i = 0; i < 8; i++)
    {
        DS_READ;

        data |= DS_STATE << i;
        _delay_us(60);
    }

    return data;
}

uint8_t
DS18B20::
CRCStep(uint8_t shift, uint8_t input)
{
    uint8_t tmp = shift & 1;

    shift >>= 1;
    shift |= (tmp ^ input) << 7;
    shift ^= (shift >> 4) & (1 << 3);
    shift ^= (shift >> 5) & (1 << 2);

    return shift;
}


int16_t
DS18B20::
ReadTemp()
{
    if (!Reset())
       return 20001;

    Write(0xCC);
    Write(0x44);

    DS_PULL;
    _delay_ms(750);
    DS_FREE;

    if (!Reset())
       return 20002;

    Write(0xCC);
    Write(0xBE);

    uint8_t crc = 0;

    uint8_t lsb = Read();
    for (uint8_t i = 0; i < 8; i++)
    crc = CRCStep(crc, (lsb & (1 << i)) ? 1 : 0);

    uint8_t msb = Read();
    for (uint8_t i = 0; i < 8; i++)
    crc = CRCStep(crc, (msb & (1 << i)) ? 1 : 0);

    for (uint8_t i = 0; i < 6; i++)
    {
        uint8_t data = Read();
        for (uint8_t i = 0; i < 8; i++)
        crc = CRCStep(crc, (data & (1 << i)) ? 1 : 0);
    }

    if (Read() != crc)
         return 20003;

    uint16_t data = (msb << 8) | lsb;

    // Если отрицательное - перевести из обратного кода
    if (msb >> 7)
        data = ~data + 1;

    return (data >> 4);

    // А это по правильному, но не работает
    //return ((msb >> 7) ? -1 : 1) * (((data >> 4) & 0x7F) * 100 + ((data & 0xF) * 625) / 100);
    // А так если нужнл в плавающем виде
    //float result = (float)(_lsb + (_msb * 256)) / 16;
}

