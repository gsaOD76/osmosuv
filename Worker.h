/*
 * Worker.h
 *
 * Created: 30.03.2020 21:41:49
 *  Author: gsaod
 */


#ifndef WORKER_H_
#define WORKER_H_

#include "SevSegDynamic.h"
#include "DS1302.h"
#include "DS18B20.h"
#include "Timer_1.h"
#include "Filter.h"

class Worker
{
    public:
        Worker(SevSegDynamic* pdisplay, DS1302* prtc, DS18B20* pts, Timer_1* pflowCounter);
        ~Worker();

        void displayRefresh();
        void resetRTC();
        void getTemp();
        void getRTC();
        void displayNextValue();
        void updateCounters();
        void incAmounts();
        void factoryReset();
        void offDisplay();
        void updateFromNvRam();
        void recalculationReminder(uint16_t before);

    private:
        enum dValue
        {
            dRevision = 1,
            dTime = 2,
            dDayWaterCounter = 3,
            dTemp = 4,
            dF123 = 5,
            dFpost = 6,
            dMembrane = 7,
            dLamp = 8,
            dLastItem = 9
        };

        SevSegDynamic* _pDisplay;
        DS1302*  _prtc;
        DS18B20* _pTermoSensor;
        Timer_1* _pflowCounter;

        // ������� �����
        Time     _realTime;

        // ������� ����������� �����
        int16_t  _lampCurrentTemp;

        // ����� ���������� �����
        int16_t  _lampMaxTemp;

        // ��� ���������� ��������, ������������� �� �������
        uint8_t _lastDispValue;

        // ������� ���� ����, ��������
        uint16_t _currentFlowCount;

        // ��������� ����� (��������/���������)
        bool _lampPowerState;

        // ������� ������ �����
        uint16_t _lampSecsCounter;

        // ������� ������ ����� �������� �������� �����������������
        uint16_t _lampSecsCheckCounter;

        // ������ �����
        uint8_t _nigthStart;
        uint8_t _nigthStop;
        uint8_t _nigthEnable;
        uint8_t _lampEnable;

        // ������ 123 �������
        Filter _filter123;

        // ������ ����������
        Filter _filterPostCarbone;

        // ��������
        Filter _filterMembrane;

        // �����
        Filter _lampUV;

        // ���� ��������������, ������������ ��� ������ ������� ����������� ���������
        bool  _alarmFlag;

        // ���� ������ �����
        bool  _lampFailFlag;

        // ���� ��������� �����������
        bool  _lampTempChange;

        uint16_t _dayWaterCounter;

        uint16_t _flowCalibrateValue;

        // ����� ������ ��������, ����
        uint16_t _dayMembraneCounter;
};

#endif /* WORKER_H_ */