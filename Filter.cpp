/*
 * Filter.cpp
 *
 * Created: 03.04.2020 22:53:15
 *  Author: gsaod
 */

#include "Filter.h"
#include <avr/eeprom.h>

void
Filter::
init(uint8_t nvramAdr, uint16_t eepromAdr, uint16_t defaultLimit, DS1302* prtc)
{
    _nvramAdr = nvramAdr;
    _eepromAdr = eepromAdr;
    _defaultLimit = defaultLimit;
    _prtc = prtc;
    _limit =  eeprom_read_word((uint16_t*)eepromAdr);
    _amount = _prtc->readRamWord(nvramAdr);

    makeReminder();
}

void
Filter::
makeReminder()
{
    uint16_t perPercent = _limit / static_cast<uint16_t>(100);

    uint16_t progress = _amount / perPercent;

    if (progress > 100)
        progress = 100;
    else if (progress == 0)
        progress = 1;

    _remainder = 100 - static_cast<uint8_t>(progress);
}


void
Filter::
nextAmountValue()
{
    _amount++;

    if (_amount > _limit)
        _amount = _limit;

    _prtc->writeRamWord(_nvramAdr, _amount);

    makeReminder();
}

void
Filter::
factoryReset()
{
    _amount = 0;
    _prtc->writeRamWord(_nvramAdr, 0);
    eeprom_write_word((uint16_t*)_eepromAdr, _defaultLimit);

    _remainder = 99;
}

void
Filter::
reset()
{
    _amount = 0;
    _prtc->writeRamWord(_nvramAdr, 0);
    _remainder = 99;
}

void
Filter::
updateFromNvRam()
{
    _amount = _prtc->readRamWord(_nvramAdr);
    makeReminder();
}

void
Filter::
setAmount(uint16_t value)
{
    _amount = value;
    _prtc->writeRamWord(_nvramAdr, value);
}