/*
 * Filter.h
 *
 * Created: 03.04.2020 22:48:19
 *  Author: gsaod
 */


#ifndef FILTER_H_
#define FILTER_H_

#include "DS1302.h"

class Filter
{
    public:
        Filter() :
        _amount(0),
        _remainder(0),
        _limit(0),
        _nvramAdr(0),
        _eepromAdr(0),
        _defaultLimit(0),
        _prtc(NULL)
        {};

        void init(uint8_t nvramAdr, uint16_t eepromAdr, uint16_t defaultLimit, DS1302* prtc);
        void nextAmountValue();
        void factoryReset();
        void reset();
        void updateFromNvRam();
        uint16_t getAmount() const { return _amount; }
        uint8_t  getRemainder() const { return _remainder; }
        uint16_t  getLimit() const { return _limit; }

        void setAmount(uint16_t value);

    private:
        void makeReminder();

        uint16_t _amount;
        uint8_t  _remainder;
        uint16_t _limit;
        uint8_t  _nvramAdr;
        uint16_t _eepromAdr;
        uint16_t _defaultLimit;
        DS1302*  _prtc;
};

#endif /* FILTER_H_ */