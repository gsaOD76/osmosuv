/*
 * conf_adr_mem.h
 *
 * Created: 02.04.2020 19:05:08
 *  Author: gsaod
 */


#ifndef CONF_ADR_MEM_H_
#define CONF_ADR_MEM_H_

// ������ ����� NVRAM ������ �����, � ������� ����� �������� ���������� ��������
#define ADR_NVRAM_MEMBRANE   0
#define ADR_NVRAM_F_123      2
#define ADR_NVRAM_F_POST     4
#define ADR_NVRAM_LAMP       6

// �������� �������
#define ADR_NVRAM_DAY_CNT    8

// ������� �������
#define ADR_NVRAM_CURRENT_CNT     10

// ������� ���� ������ ������� ������
#define ADR_NVRAM_DAY_MEMBRANE_CNT   12

// ������������� �������� �������, ��������� �� ����, �� ��������� 2125 (2 �����)
#define ADR_EEPROM_FLOW_CAL        0
#define DEF_EEPROM_FLOW_CAL        2125

// ����� �������� ��������� ������, ������, �� ��������� 3500
#define ADR_EEPROM_MEMBRANE_LIMIT  4
#define DEF_EEPROM_MEMBRANE_LIMIT  3500

// ����� �������� 1-3 ������� ������, �� ��������� 875
#define ADR_EEPROM_F_123_LIMIT     6
#define DEF_EEPROM_F_123_LIMIT     875

// ����� �����������, ������, �� ��������� 1750
#define ADR_EEPROM_F_POST_LIMIT    8
#define DEF_EEPROM_F_POST_LIMIT    1750

// ����� ������� ������ �����, �����, �� ��������� 8000
#define ADR_EEPROM_LAMP_LIMIT      10
#define DEG_EEPROM_LAMP_LIMIT      8000

// ����� ������ ������� ������, ����, �� ��������� 0
#define ADR_EEPROM_NIGTH_START     12
#define DEF_EEPROM_NIGTH_START     0

// ����� ���������� ������� ������, ����, �� ��������� 7
#define ADR_EEPROM_NIGTH_STOP      13
#define DEF_EEPROM_NIGTH_STOP      7

#define ADR_EEPROM_NIGTH_ENABLE    14
#define DEF_EEPROM_NIGTH_ENABLE    1

// ����� ������ �������� �� ���������
#define ADR_EEPROM_DAY_BEFORE      16
#define DEF_EEPROM_DAY_BEFORE      0

#define ADR_EEPROM_LAMP_ENABLE     18
#define DEF_EEPROM_LAMP_ENABLE     1

#endif /* CONF_ADR_MEM_H_ */