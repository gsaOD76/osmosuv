/*
 * Menu.h
 *
 * Created: 05.04.2020 18:47:57
 *  Author: gsaod
 */


#ifndef MENU_H_
#define MENU_H_


#include "SevSegDynamic.h"
#include "DS1302.h"
#include "Timer_1.h"
#include "Filter.h"

class Menu
{
    public:
        enum mMode
        {
            modeWork  = 0,
            modeMenu = 1
        };

        Menu(SevSegDynamic* pdisplay, DS1302* prtc, Timer_1* pflowCounter);
        ~Menu();

        void init();
        void displayRefresh();
        uint8_t doFSM(bool shortPush, bool longPush);

    private:
        enum mValue
        {
            sExit = 0,
            sTimeView = 1,
            sHourSet = 2,
            sMinuteSet = 3,
            sF123 = 4,
            sFpost = 5,
            sMembrane = 6,
            sLamp = 7,
            sCalibrate = 8,
            sCalibrateProcess = 9,
            sNigthBegin = 10,
            sNigthBeginSet = 11,
            sNigthMode = 12,
            sLampMode = 13,
            sDayTotal = 14,
            sDayTotalSet = 15
        };

        typedef uint8_t(Menu::*FSM)(bool,bool);

        void doubleBeep();
        static void uintToChar(uint16_t input, char *Res);

        // ������ ���������� �� ����������� ��������� ��������� ��������
        uint8_t (Menu::*ptrStateHandler[16])(bool,bool)= {&Menu::stateExit, &Menu::stateTimeView, &Menu::stateHourSet, &Menu::stateMinuteSet,
            &Menu::stateF123, &Menu::stateFPost, &Menu::stateMembrane, &Menu::stateLamp, &Menu::stateCalibrate, &Menu::stateCalibrateProcess,
            &Menu::stateNigthBegin, &Menu::stateNigthBeginSet, &Menu::stateNigthMode,  &Menu::stateLampMode, &Menu::stateDayTotal, &Menu::stateDayTotalSet};

        uint8_t stateExit(bool shortPush, bool longPush);
        uint8_t stateTimeView(bool shortPush, bool longPush);
        uint8_t stateHourSet(bool shortPush, bool longPush);
        uint8_t stateMinuteSet(bool shortPush, bool longPush);
        uint8_t stateF123(bool shortPush, bool longPush);
        uint8_t stateFPost(bool shortPush, bool longPush);
        uint8_t stateMembrane(bool shortPush, bool longPush);
        uint8_t stateLamp(bool shortPush, bool longPush);
        uint8_t stateCalibrate(bool shortPush, bool longPush);
        uint8_t stateCalibrateProcess(bool shortPush, bool longPush);

        uint8_t stateNigthBegin(bool shortPush, bool longPush);
        uint8_t stateNigthBeginSet(bool shortPush, bool longPush);
        uint8_t stateNigthMode(bool shortPush, bool longPush);
        uint8_t stateLampMode(bool shortPush, bool longPush);

        uint8_t stateDayTotal(bool shortPush, bool longPush);
        uint8_t stateDayTotalSet(bool shortPush, bool longPush);

        SevSegDynamic* _pDisplay;
        DS1302*  _prtc;
        Timer_1* _pflowCounter;

        // ������� �����
        Time     _realTime;

        // ������ 123 �������
        Filter _filter123;

        // ������ ����������
        Filter _filterPostCarbone;

        // ��������
        Filter _filterMembrane;

        // �����
        Filter _lampUV;

        // ���������
        uint8_t _stateFSM;

        char _displayBuffer[6];

        // ������ �����
        uint8_t _nigthStart;
        uint8_t _nigthStop;
        uint8_t _nigthEnable;

        uint8_t _lampEnable;

        uint16_t _dayTotal;
        uint16_t _dayTotalBefore;

};
#endif /* MENU_H_ */