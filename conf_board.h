/*
 * conf_board.h
 *
 * Created: 27.03.2020 21:25:31
 *  Author: gsaod
 */


#ifndef CONF_BOARD_H_
#define CONF_BOARD_H_

#define  REVISION   "r-1.2"

// Порог температуры для выключения лампы
#define MAX_LAMP_TEMP     40

// Гистерезис включения температуры, градусов
#define  TEMP_HYSTERESIS  3

// Лимит остатка для звукового сигнала, процентов
#define  BEEP_LIMIT       5

// Время проверки лампы на работоспособность, секунд
#define  LAMP_CHECK_TIMEOUT   1200

// Сегменты
#define SEG_PORT    PORTA
#define SEG_DDR     DDRA
#define SEG_A_PIN   0
#define SEG_B_PIN   1
#define SEG_C_PIN   2
#define SEG_D_PIN   3
#define SEG_E_PIN   4
#define SEG_F_PIN   5
#define SEG_G_PIN   6
#define SEG_DP_PIN  7

// Цифры
#define DIG_PORT    PORTD
#define DIG_DDR     DDRD
#define DIG_1_PIN   4
#define DIG_2_PIN   5
#define DIG_3_PIN   6
#define DIG_4_PIN   7

// DS1302
#define DS1302_PORT      PORTC
#define DS1302_DDR       DDRC
#define DS1302_PIN       PINC
#define DS1302_CE_PIN    1
#define DS1302_IO_PIN    0
#define DS1302_SCLK_PIN  2

// DS18B20
#define DS18B20_PORT     PORTD
#define DS18B20_DDR      DDRD
#define DS18B20_PIN      PIND
#define DS18B20_1W_PIN   2

// Кнопка
#define BUTTON	  C, 4

// Светодиод расхода
#define FLOW_LED  B, 3

// Светодиод работы лампы
#define LAMP_LED  B, 4

// Питание лампы
#define LAMP_PWR  C, 3

// Пищалка
#define BUZER     C, 5

// Счетный вход
#define COUNT_PULSE   B, 1


#endif /* CONF_BOARD_H_ */