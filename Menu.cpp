/*
 * Menu.cpp
 *
 * Created: 05.04.2020 19:04:57
 *  Author: gsaod
 */

#include <avr/eeprom.h>
#include <string.h>
#include <util/delay.h>

#include "conf_mem.h"
#include "Menu.h"

Menu::
Menu(SevSegDynamic* pdisplay, DS1302* prtc, Timer_1* pflowCounter) :
    _pDisplay(pdisplay),
    _prtc(prtc),
    _pflowCounter(pflowCounter),
    _stateFSM(sExit),
    _nigthStart(0),
    _nigthStop(0),
    _nigthEnable(0),
    _lampEnable(1),
    _dayTotal(0),
    _dayTotalBefore(0)
{

}

Menu::
~Menu()
{

}

void
Menu::
init()
{
    doubleBeep();

    // ������� � ���������������� ��������
    _filter123.init(ADR_NVRAM_F_123, ADR_EEPROM_F_123_LIMIT, DEF_EEPROM_F_123_LIMIT, _prtc);
    _filterPostCarbone.init(ADR_NVRAM_F_POST, ADR_EEPROM_F_POST_LIMIT, DEF_EEPROM_F_POST_LIMIT, _prtc);
    _filterMembrane.init(ADR_NVRAM_MEMBRANE, ADR_EEPROM_MEMBRANE_LIMIT, DEF_EEPROM_MEMBRANE_LIMIT, _prtc);
    _lampUV.init(ADR_NVRAM_LAMP, ADR_EEPROM_LAMP_LIMIT, DEG_EEPROM_LAMP_LIMIT, _prtc);

    _nigthStart = eeprom_read_byte((uint8_t*)ADR_EEPROM_NIGTH_START);
    _nigthStop = eeprom_read_byte((uint8_t*)ADR_EEPROM_NIGTH_STOP);
    _nigthEnable = eeprom_read_byte((uint8_t*)ADR_EEPROM_NIGTH_ENABLE);

    _lampEnable = eeprom_read_byte((uint8_t*)ADR_EEPROM_LAMP_ENABLE);

    _dayTotal = _prtc->readRamWord(ADR_NVRAM_DAY_MEMBRANE_CNT);

    _realTime = _prtc->time();

    _stateFSM = sExit;

    memset(_displayBuffer, 0, 6);

    _displayBuffer[0] = 'E';
    _displayBuffer[1] = '-';
    _displayBuffer[2] = '-';
    _displayBuffer[3] = '-';

    _pDisplay->setText(_displayBuffer);
}

void
Menu::
displayRefresh()
{
    // ���� ���� ���������� - ���������� ��������� ������� ��������
    if (_stateFSM == sCalibrateProcess)
    {
        memset(_displayBuffer, 0, 6);
        uint16_t pulseCount = _pflowCounter->getCount();
        uintToChar(pulseCount, _displayBuffer);
        _pDisplay->setText(_displayBuffer);
    }

    _pDisplay->refresh();
}

uint8_t
Menu::
doFSM(bool shortPush, bool longPush)
{
    return (this->*ptrStateHandler[_stateFSM])(shortPush,longPush);
}

uint8_t
Menu::
stateExit(bool shortPush, bool longPush)
{
    // ���� �������� ������� - ������� � ����� ��������� �������, ������� �����
    if (shortPush)
    {
        _stateFSM = sTimeView;

        memset(_displayBuffer, 0, 6);

        _displayBuffer[0] = (_realTime._hr / 10) + 0x30;
        _displayBuffer[1] = (_realTime._hr % 10) + 0x30;
        _displayBuffer[2] = '.';
        _displayBuffer[3] = (_realTime._min / 10) + 0x30;
        _displayBuffer[4] = (_realTime._min % 10) + 0x30;

        _pDisplay->setText(_displayBuffer);
    }
    // ����� ����� � ������� �����
    else if (longPush)
    {
        doubleBeep();
        return modeWork;
    }

    return modeMenu;
}

uint8_t
Menu::
stateTimeView(bool shortPush, bool longPush)
{
    memset(_displayBuffer, 0, 6);

    // ���� �������� ������� - ������� � ����� ��������� ������� ������� 1-2-3
    if (shortPush)
    {
        _stateFSM = sF123;

        // ������� ������ �������
        _displayBuffer[0] = 'F';
        _displayBuffer[1] = '-';
        _displayBuffer[2] = (_filter123.getRemainder() / 10) + 0x30;
        _displayBuffer[3] = (_filter123.getRemainder()  % 10) + 0x30;
    }
    // ����� ����� ��������� �����
    else if (longPush)
    {
        _stateFSM = sHourSet;

        _displayBuffer[0] = (_realTime._hr / 10) + 0x30;
        _displayBuffer[1] = (_realTime._hr % 10) + 0x30;
        _displayBuffer[2] = '.';
        _displayBuffer[3] = ' ';
        _displayBuffer[4] = ' ';
    }

    _pDisplay->setText(_displayBuffer);

    return modeMenu;
}

uint8_t
Menu::
stateHourSet(bool shortPush, bool longPush)
{
    memset(_displayBuffer, 0, 6);

    // ���� �������� ������� - ��������� ����
    if (shortPush)
    {
        if (++_realTime._hr > 23)
        _realTime._hr = 0;

        _displayBuffer[0] = (_realTime._hr / 10) + 0x30;
        _displayBuffer[1] = (_realTime._hr % 10) + 0x30;
        _displayBuffer[2] = '.';
        _displayBuffer[3] = ' ';
        _displayBuffer[4] = ' ';
    }
    // ����� ����� ��������� �����
    else if (longPush)
    {
        _stateFSM = sMinuteSet;

        // ������� ������
        _displayBuffer[0] = ' ';
        _displayBuffer[1] = ' ';
        _displayBuffer[2] = '.';
        _displayBuffer[3] = (_realTime._min / 10) + 0x30;
        _displayBuffer[4] = (_realTime._min % 10) + 0x30;
    }

    _pDisplay->setText(_displayBuffer);

    return modeMenu;
}


uint8_t
Menu::
stateMinuteSet(bool shortPush, bool longPush)
{
    memset(_displayBuffer, 0, 6);

    // ���� �������� ������� - ��������� ������
    if (shortPush)
    {
        if (++_realTime._min > 59)
        _realTime._min = 0;

        _displayBuffer[0] = ' ';
        _displayBuffer[1] = ' ';
        _displayBuffer[2] = '.';
        _displayBuffer[3] = (_realTime._min / 10) + 0x30;
        _displayBuffer[4] = (_realTime._min % 10) + 0x30;
    }
    // ���� ������� ������� - ������� � ����� ��������� ������� ������� 1-2-3
    else if (longPush)
    {
        doubleBeep();

        // ���������� �����
        _prtc->time(_realTime);

        _stateFSM = sF123;

        // ������� ������ �������
        _displayBuffer[0] = 'F';
        _displayBuffer[1] = '-';
        _displayBuffer[2] = (_filter123.getRemainder() / 10) + 0x30;
        _displayBuffer[3] = (_filter123.getRemainder()  % 10) + 0x30;
    }

    _pDisplay->setText(_displayBuffer);

    return modeMenu;
}

uint8_t
Menu::
stateF123(bool shortPush, bool longPush)
{
    memset(_displayBuffer, 0, 6);

    // ���� �������� ������� - ������� � ����� ������� ����������
    if (shortPush)
    {
        _stateFSM = sFpost;

        // ������� ������ �������
        _displayBuffer[0] = 'P';
        _displayBuffer[1] = '-';
        _displayBuffer[2] = (_filterPostCarbone.getRemainder() / 10) + 0x30;
        _displayBuffer[3] = (_filterPostCarbone.getRemainder()  % 10) + 0x30;
    }
    // ����� �������� ������ �������
    else if (longPush)
    {
        doubleBeep();

        // �������� ������ �������
        _filter123.reset();

        // ������� ������ �������
        _displayBuffer[0] = 'F';
        _displayBuffer[1] = '-';
        _displayBuffer[2] = (_filter123.getRemainder() / 10) + 0x30;
        _displayBuffer[3] = (_filter123.getRemainder()  % 10) + 0x30;
    }

    _pDisplay->setText(_displayBuffer);

    return modeMenu;
}

uint8_t
Menu::
stateFPost(bool shortPush, bool longPush)
{
    memset(_displayBuffer, 0, 6);

    // ���� �������� ������� - ������� � ����� ��������
    if (shortPush)
    {
        _stateFSM = sMembrane;

        // ������� ������ �������
        _displayBuffer[0] = 'H';
        _displayBuffer[1] = '-';
        _displayBuffer[2] = (_filterMembrane.getRemainder() / 10) + 0x30;
        _displayBuffer[3] = (_filterMembrane.getRemainder()  % 10) + 0x30;
    }
    // ����� �������� ������ �������
    else if (longPush)
    {
        doubleBeep();

        // �������� ������ �������
        _filterPostCarbone.reset();

        // ������� ������ �������
        _displayBuffer[0] = 'P';
        _displayBuffer[1] = '-';
        _displayBuffer[2] = (_filterPostCarbone.getRemainder() / 10) + 0x30;
        _displayBuffer[3] = (_filterPostCarbone.getRemainder()  % 10) + 0x30;
    }

    _pDisplay->setText(_displayBuffer);

    return modeMenu;
}

uint8_t
Menu::
stateMembrane(bool shortPush, bool longPush)
{
    memset(_displayBuffer, 0, 6);

    // ���� �������� ������� - ������� � ����� �����
    if (shortPush)
    {
        _stateFSM = sLamp;

        // ������� ������ �����
        _displayBuffer[0] = 'L';
        _displayBuffer[1] = '-';
        _displayBuffer[2] = (_lampUV.getRemainder() / 10) + 0x30;
        _displayBuffer[3] = (_lampUV.getRemainder()  % 10) + 0x30;
    }
    // ����� �������� ������ �������
    else if (longPush)
    {
        doubleBeep();

        // �������� ������ �������
        _filterMembrane.reset();
        _prtc->writeRamWord(ADR_NVRAM_DAY_MEMBRANE_CNT, 0);

        // ������� ������ �������
        _displayBuffer[0] = 'H';
        _displayBuffer[1] = '-';
        _displayBuffer[2] = (_filterMembrane.getRemainder() / 10) + 0x30;
        _displayBuffer[3] = (_filterMembrane.getRemainder()  % 10) + 0x30;
    }

    _pDisplay->setText(_displayBuffer);

    return modeMenu;
}

uint8_t
Menu::
stateLamp(bool shortPush, bool longPush)
{
    memset(_displayBuffer, 0, 6);

    // ���� �������� ������� - ������� � ����� ����������
    if (shortPush)
    {
        _stateFSM = sCalibrate;

        _displayBuffer[0] = 'C';
        _displayBuffer[1] = 'A';
        _displayBuffer[2] = 'L';
        _displayBuffer[3] = ' ';
    }
    // ����� �������� ������ �������
    else if (longPush)
    {
        doubleBeep();

        // �������� ������ �������
        _lampUV.reset();

        // ������� ������ ������
        _displayBuffer[0] = 'L';
        _displayBuffer[1] = '-';
        _displayBuffer[2] = (_lampUV.getRemainder() / 10) + 0x30;
        _displayBuffer[3] = (_lampUV.getRemainder()  % 10) + 0x30;
    }

    _pDisplay->setText(_displayBuffer);

    return modeMenu;
}

uint8_t
Menu::
stateCalibrate(bool shortPush, bool longPush)
{
    memset(_displayBuffer, 0, 6);

    // ���� �������� ������� - ������� � ����� �������� � ����
    if (shortPush)
    {
        _stateFSM = sNigthBegin;

        _displayBuffer[0] = 'n';
        _displayBuffer[1] = '-';
        _displayBuffer[2] = (_nigthStart / 10) + 0x30;
        _displayBuffer[3] = (_nigthStart % 10) + 0x30;
    }
    // ����� ��������� �����������
    else if (longPush)
    {
        doubleBeep();

        _stateFSM = sCalibrateProcess;

        // ���������� ������� � ����
        _pflowCounter->set(0x0000);

        // ������� ��������� � ������������ ��������
        _pflowCounter->setCompareValue(0xFFFF);
    }

    _pDisplay->setText(_displayBuffer);

    return modeMenu;
}

uint8_t
Menu::
stateCalibrateProcess(bool shortPush, bool longPush)
{
    if (longPush)
    {
        doubleBeep();

        // �������� ������������� ��������
        uint16_t pulseCount = _pflowCounter->getCount();

        // �������� ������� � ����
         _pflowCounter->set(0x0000);

        // �������� ���������� ������������� �������� � ������� ���������
        _pflowCounter->setCompareValue(pulseCount);

        cli();
        // ��������� � EEPROM ������������� ��������
        eeprom_write_word((uint16_t*)ADR_EEPROM_FLOW_CAL, pulseCount);
        sei();

        _stateFSM = sNigthBegin;

        _displayBuffer[0] = 'n';
        _displayBuffer[1] = '-';
        _displayBuffer[2] = (_nigthStart / 10) + 0x30;
        _displayBuffer[3] = (_nigthStart % 10) + 0x30;
    }

    return modeMenu;
}

uint8_t
Menu::
stateNigthBegin(bool shortPush, bool longPush)
{
    memset(_displayBuffer, 0, 6);

    // ���� �������� ������� - ������� � ����� ������ ��������� ������� ������
    if (shortPush)
    {
        _stateFSM = sNigthMode;

        _displayBuffer[0] = 'n';
        _displayBuffer[1] = '-';

        if (_nigthEnable)
        {
            _displayBuffer[2] = 'o';
            _displayBuffer[3] = 'n';
        }
        else
        {
            _displayBuffer[2] = 'o';
            _displayBuffer[3] = 'F';
        }
    }
    else if (longPush)
    {
        _stateFSM = sNigthBeginSet;

        _displayBuffer[0] = ' ';
        _displayBuffer[1] = ' ';
        _displayBuffer[2] = '.';
        _displayBuffer[3] = (_nigthStart / 10) + 0x30;
        _displayBuffer[4] = (_nigthStart % 10) + 0x30;
    }

    _pDisplay->setText(_displayBuffer);

    return modeMenu;
}

uint8_t
Menu::
stateNigthBeginSet(bool shortPush, bool longPush)
{
    memset(_displayBuffer, 0, 6);

    // ���� �������� ������� - ��������� ����
    if (shortPush)
    {
        if (++_nigthStart > 23)
            _nigthStart = 0;

        _displayBuffer[0] = ' ';
        _displayBuffer[1] = ' ';
        _displayBuffer[2] = '.';
        _displayBuffer[3] = (_nigthStart / 10) + 0x30;
        _displayBuffer[4] = (_nigthStart % 10) + 0x30;
    }
    else if (longPush)
    {
         // ���������
         doubleBeep();

         cli();
         eeprom_write_byte((uint8_t*)ADR_EEPROM_NIGTH_START, _nigthStart);
         sei();

         _stateFSM = sNigthMode;

         _displayBuffer[0] = 'n';
         _displayBuffer[1] = '-';

         if (_nigthEnable)
         {
             _displayBuffer[2] = 'o';
             _displayBuffer[3] = 'n';
         }
         else
         {
             _displayBuffer[2] = 'o';
             _displayBuffer[3] = 'F';
         }
    }

    _pDisplay->setText(_displayBuffer);

    return modeMenu;
}

 uint8_t
 Menu::
 stateNigthMode(bool shortPush, bool longPush)
 {
    memset(_displayBuffer, 0, 6);

    if (shortPush)
    {
        _stateFSM = sLampMode;

         _displayBuffer[0] = 'u';
         _displayBuffer[1] = '-';

         if (_lampEnable)
         {
             _displayBuffer[2] = 'o';
             _displayBuffer[3] = 'n';
         }
         else
         {
             _displayBuffer[2] = 'o';
             _displayBuffer[3] = 'F';
         }
    }
    else if (longPush)
    {
        _displayBuffer[0] = 'n';
        _displayBuffer[1] = '-';

        if (_nigthEnable == 0)
        {
            _nigthEnable = 1;
            _displayBuffer[2] = 'o';
            _displayBuffer[3] = 'n';
        }
        else if (_nigthEnable == 1)
        {
            _nigthEnable = 0;
            _displayBuffer[2] = 'o';
            _displayBuffer[3] = 'F';
        }

        // ���������
        doubleBeep();

        cli();
        eeprom_write_byte((uint8_t*)ADR_EEPROM_NIGTH_ENABLE, _nigthEnable);
        sei();
    }

    _pDisplay->setText(_displayBuffer);

    return modeMenu;
 }

  uint8_t
  Menu::
  stateLampMode(bool shortPush, bool longPush)
  {
      memset(_displayBuffer, 0, 6);

      if (shortPush)
      {
          _stateFSM = sDayTotal;

          uint16_t dayCount = _dayTotal;
          uintToChar(dayCount, _displayBuffer);
          _displayBuffer[0] = 'd';
      }
      else if (longPush)
      {
          _displayBuffer[0] = 'u';
          _displayBuffer[1] = '-';

          if (_lampEnable == 0)
          {
              _lampEnable = 1;
              _displayBuffer[2] = 'o';
              _displayBuffer[3] = 'n';
          }
          else if (_lampEnable > 0)
          {
              _lampEnable = 0;
              _displayBuffer[2] = 'o';
              _displayBuffer[3] = 'F';
          }

          // ���������
          doubleBeep();

          cli();
          eeprom_write_byte((uint8_t*)ADR_EEPROM_LAMP_ENABLE, _lampEnable);
          sei();
      }

      _pDisplay->setText(_displayBuffer);

      return modeMenu;
  }

 uint8_t
 Menu::
 stateDayTotal(bool shortPush, bool longPush)
 {
     memset(_displayBuffer, 0, 6);

     if (shortPush)
     {
         _stateFSM = sExit;

         _displayBuffer[0] = 'E';
         _displayBuffer[1] = '-';
         _displayBuffer[2] = '-';
         _displayBuffer[3] = '-';
     }
     else if (longPush)
     {
           _stateFSM  = sDayTotalSet;

           uint16_t dayCount = _dayTotal;
           uintToChar(dayCount, _displayBuffer);
           _displayBuffer[0] = ' ';
           _dayTotalBefore = _dayTotal;
     }

     _pDisplay->setText(_displayBuffer);

     return modeMenu;
 }

  uint8_t
  Menu::
  stateDayTotalSet(bool shortPush, bool longPush)
  {
      memset(_displayBuffer, 0, 6);

      if (shortPush)
      {
          _dayTotalBefore += 10;
          if (_dayTotalBefore > 360)
              _dayTotalBefore = 0;

          uint16_t dayCountBefore = _dayTotalBefore;
          uintToChar(dayCountBefore, _displayBuffer);
          _displayBuffer[0] = ' ';
      }
      else if (longPush)
      {
         // ���������
         doubleBeep();

         cli();
         eeprom_write_word((uint16_t*)ADR_EEPROM_DAY_BEFORE, _dayTotalBefore);
         sei();

          _stateFSM = sExit;

          _displayBuffer[0] = 'E';
          _displayBuffer[1] = '-';
          _displayBuffer[2] = '-';
          _displayBuffer[3] = '-';
      }

      _pDisplay->setText(_displayBuffer);

      return modeMenu;
  }

void
Menu::
doubleBeep()
{
    _pDisplay->turnOff();

    DigitalWrite(BUZER, Low);
    _delay_ms(100);
    DigitalWrite(BUZER, High);
    _delay_ms(100);
    DigitalWrite(BUZER, Low);
    _delay_ms(100);
    DigitalWrite(BUZER, High);
}

void
Menu::
uintToChar
(uint16_t input, char *Res)
{
    uint8_t thousant = 0, hundret = 0, tens = 0;

    while (input >= 1000)
    {
        thousant++;
        input -= 1000;
    }
    *Res++ = thousant + 0x30;

    while (input >= 100)
    {
        hundret++;
        input -= 100;
    }
    *Res++ = hundret + 0x30;

    while (input >= 10)
    {
        tens++;
        input -= 10 ;
    }
    *Res++ = tens + 0x30;
    *Res = input + 0x30;
}



