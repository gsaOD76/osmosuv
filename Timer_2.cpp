/*
 * Timer_2.cpp
 *
 * Created: 28.03.2020 19:49:43
 *  Author: gsaod
 */

#include "Timer_2.h"

Timer_2::
Timer_2(T_PRESCALER prescale, T_MODE mode) : iTimer(prescale, mode)
{
    _asynMode = false;

    switch (mode)
    {
        case NORMAL :
            BitClear(TCCR2, WGM20);
            BitClear(TCCR2, WGM21);
        break;

        case CTC :
            BitClear(TCCR2, WGM20);
            BitSet(TCCR2, WGM21);
        break;

        case PWM_PC :
        case PWM_FC :
            BitSet(TCCR2, WGM20);
            BitClear(TCCR2, WGM21);
        break;

        case PWM_F :
            BitSet(TCCR2, WGM20);
            BitSet(TCCR2, WGM21);
        break;

        default : break;
    }

    switch (prescale)
    {
        TCCR2 &= 0b11111000;

        case PRESCALER_OFF :
        break;

        case PRESCALER_1 :
        TCCR2 |= 0b00000001;
        break;

        case PRESCALER_8 :
        TCCR2 |= 0b00000010;
        break;

        case PRESCALER_32 :
        TCCR2 |= 0b00000011;
        break;

        case PRESCALER_64 :
        TCCR2 |= 0b00000100;
        break;

        case PRESCALER_128 :
        TCCR2 |= 0b00000101;
        break;

        case PRESCALER_256 :
        TCCR2 |= 0b00000110;
        break;

        case PRESCALER_1024 :
        TCCR2 |= 0b00000111;
        break;

        default: break;
    }
}


void
Timer_2::
reset()
{
    TCNT2 = 0;

    if (_asynMode)
        while (ASSR & (1 << TCN2UB));
}


uint16_t
Timer_2::
getCount()
{
    if (_asynMode)
        while (ASSR & (1 << TCN2UB));

    return TCNT2;
}

void
Timer_2::
setInterrupt(T_INTERRUPT interrupt)
{
    switch (interrupt)
    {
        case NONE_I :
        break;

        case OVERFLOW :
            BitClear(TIFR, TOV2);
            BitSet(TIMSK, TOIE2);
        break;

        case COMPARE :
            BitClear(TIFR, OCF2);
            BitSet(TIMSK, OCIE2);
        break;

        default: break;
    }
}

void
Timer_2::
set(const uint16_t count)
{
    TCNT2 = count;

    if (_asynMode)
       while (ASSR & (1 << TCN2UB));
}


void
Timer_2::
setCompareValue(const uint16_t count)
{
    OCR2 = count;

    if (_asynMode)
       while (ASSR & (1 << OCR2UB));
}

void
Timer_2::
SetAsynMode()
{
    _asynMode = true;

    BitSet(ASSR, AS2);

    while (ASSR & (1 << TCR2UB));
}