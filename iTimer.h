/*
 * iTimer.h
 *
 * Created: 28.03.2020 12:19:03
 *  Author: gsaod
 */


#ifndef ITIMER_H_
#define ITIMER_H_

#include <stdint.h>
#include <stddef.h>

#include <avr/io.h>
#include <avr/interrupt.h>

#include "IO_Macros.h"

class iTimer
{
    public:

    enum T_MODE
    {
        NONE_M		= 0,
        NORMAL	    = 1,
        CTC			= 2,
        PWM_F		= 3,
        PWM_PC	    = 4,
        PWM_FC	    = 5
    };

    enum T_INTERRUPT
    {
        NONE_I		= 0,
        OVERFLOW	= 1,
        COMPARE		= 2
    };

    enum T_PRESCALER
    {
        PRESCALER_OFF           = 0,
        PRESCALER_1	        	= 1,
        PRESCALER_8	        	= 2,
        PRESCALER_32        	= 3,
        PRESCALER_64        	= 4,
        PRESCALER_128           = 5,
        PRESCALER_256       	= 6,
        PRESCALER_1024	        = 7,
        PRESCALER_EXT_FALLING	= 8,
        PRESCALER_EXT_RISING	= 9
    };

    iTimer(T_PRESCALER prescale, T_MODE mode) :
        _prescale(prescale),
        _mode(mode) {};

    virtual void reset() = 0;
    virtual uint16_t getCount() = 0;

    virtual void setInterrupt(T_INTERRUPT interrupt) = 0;
    virtual void set(const uint16_t count) = 0;
    virtual void setCompareValue(const uint16_t count) = 0;


    protected:

    T_PRESCALER _prescale;
    T_MODE _mode;
};

#endif /* ITIMER_H_ */