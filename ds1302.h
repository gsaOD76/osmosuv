/*
 * ds1302.h
 *
 * Created: 29.03.2020 12:07:41
 *  Author: gsaod
 */


#ifndef DS1302_H_
#define DS1302_H_

#include <stdint.h>
#include <stddef.h>

#include <avr/io.h>

#include "IO_Macros.h"
#include "conf_board.h"
#include "Time.h"

class DS1302
{
    public:
    static const int kRamSize = 31;

    DS1302(uint8_t ce_pin, uint8_t io_pin, uint8_t sclk_pin);
    void writeProtect(bool enable);
    void halt(bool value);
    Time time();
    void time(Time t);
    void writeRam(uint8_t address, uint8_t value);
    void writeRamWord(uint8_t address, uint16_t value);
    uint8_t readRam(uint8_t address);
    uint16_t readRamWord(uint8_t address);
    void writeRamBulk(const uint8_t* data, int len);
    void readRamBulk(uint8_t* data, int len);
    uint8_t readRegister(uint8_t reg);
    void writeRegister(uint8_t reg, uint8_t value);

    private:

    void writeOut(uint8_t value, bool readAfter = false);
    uint8_t readIn();

    uint8_t _ce_pin;
    uint8_t _io_pin;
    uint8_t _sclk_pin;
};





#endif /* DS1302_H_ */