/*
 * DS18B20.h
 *
 * Created: 29.03.2020 19:48:19
 *  Author: gsaod
 */


#ifndef DS18B20_H_
#define DS18B20_H_

#include <stdio.h>
#include <util/delay.h>
#include <stdint.h>
#include <stddef.h>

#include "IO_Macros.h"
#include "conf_board.h"

class DS18B20
{
    public:
    DS18B20(uint8_t owi_pin);
    int16_t ReadTemp();

    private:

    uint8_t CRCStep(uint8_t shift, uint8_t input);
    void Write(uint8_t data);
    uint8_t Read();
    bool Reset();

    uint8_t _owi_pin;
};


#endif /* DS18B20_H_ */