/*
 * OsmosUV.cpp
 *
 * Created: 26.03.2020 17:17:45
 * Author : gsaod
 */

// Тактовая частота - задана в настройках проекта.
// Если собирается с командной строки и без соотв. ключа - раскоментировать
//#define F_CPU       16000000UL

#include <avr/io.h>
#include <avr/eeprom.h>
#include <avr/wdt.h>

#include "IO_Macros.h"

#include "conf_board.h"
#include "Timer_0.h"
#include "Timer_1.h"
#include "Timer_2.h"
#include "Time.h"

#include "Worker.h"
#include "Menu.h"

volatile bool flag_200_HZ = false;
volatile bool flag_1_Sec = false;
volatile bool flag_flow_count = false;

volatile bool buttonShortPushed = false;
volatile bool buttonLongPushed = false;


// Обработчик прерывания сравнения таймера 0
SIGNAL(TIMER0_COMP_vect)
{
    uint8_t buttonState = !DigitalRead(BUTTON);
    static uint16_t buttonPushCounter = 0;
    static bool buttonReleased = true;
    static bool buzerState = false;

    // Установить флаг, в прерывании ничего делать не нужно
    flag_200_HZ = true;

    if (buttonShortPushed || buttonLongPushed)
        return;

    // Если нажата кнопка
    if (buttonState && buttonReleased)
    {
        // Увеличить счетчик нажатия конпки
        buttonPushCounter++;

        // Кнопка нажата не случайно, прошла 1/10 секунды, включить звук
        if (buttonPushCounter == 20)
        {
            DigitalWrite(BUZER, Low);
            buzerState = true;
        }
        // Кнопка все еще нажата, выключить звук, это короткое нажатие
        else if (buttonPushCounter == 30)
        {
            DigitalWrite(BUZER, High);
            buzerState = false;
        }
        // Если кнопка зажата более 3 секунд - длительное нажатие
        else if (buttonPushCounter >= 600)
        {
            buttonShortPushed = false;
            buttonLongPushed = true;
            buttonPushCounter = 0;
            buttonReleased = false;
        }
    }
    // Кнопка не нажата
    else if (!buttonState)
    {
        buttonReleased = true;

        // Если кнопка была зажата недолго
        if ((buttonPushCounter > 20) && (buttonPushCounter < 600))
        {
            buttonShortPushed = true;
            buttonLongPushed = false;
        }

        // Если был включен звук - выключить
        if (buzerState)
        {
            DigitalWrite(BUZER, High);
            buzerState = false;
        }

        // Сбросить счетчик
        buttonPushCounter = 0;
    }

}

// Обработчик прерывания сравнения таймера 0
SIGNAL(TIMER2_COMP_vect)
{
    // Установить флаг, в прерывании ничего делать не нужно
    flag_1_Sec = true;
}

// Обработчик прерывания сравнения А таймера 1
SIGNAL(TIMER1_COMPA_vect)
{
    // Установить флаг счета жидкости
    flag_flow_count = true;
}


void initPorts()
{
    PinMode(COUNT_PULSE, Input);
    DigitalWrite(COUNT_PULSE, High);

    PinMode(BUTTON, Input);
    DigitalWrite(BUTTON, High);

    PinMode(FLOW_LED, Output);
    DigitalWrite(FLOW_LED, Low);

    PinMode(LAMP_LED, Output);
    DigitalWrite(LAMP_LED, Low);

    PinMode(LAMP_PWR, Output);
    DigitalWrite(LAMP_PWR, Low);

    PinMode(BUZER, Output);
    DigitalWrite(BUZER, High);
}

void initTimers()
{
    // Таймер 0 используется для обновления дисплея, тактовая частота деления на 1024, режим сброс при совпадении
    Timer_0 timerDisplay(iTimer::PRESCALER_1024, iTimer::CTC);

    // Установить регистром сравнения частоту срабатывания в 200 герц (50 герц на цифру)
    uint16_t div0 = F_CPU / 1024 / 200;
    timerDisplay.setCompareValue(static_cast<uint8_t>(div0));

    // Вызывать прерывание при сравнении
    timerDisplay.setInterrupt(iTimer::COMPARE);

    // Таймер 2 используется для счета врeмени, тактовая частота деления на 1024, режим сброс при совпадении
    Timer_2 timerSecond(iTimer::PRESCALER_1024, iTimer::CTC);

    // Перевести в асинхронный режим работы, счетчик работает от часового кварцевого резонатора (32768 кГц)
    timerSecond.SetAsynMode();

    // Установить регистром сравнения период срабатывания в 1 секунду
    timerSecond.setCompareValue(31);

    // Вызывать прерывание при сравнении
    timerSecond.setInterrupt(iTimer::COMPARE);
}

int main(void)
{
    // Включить вачдог таймер
    wdt_enable(WDTO_2S);

    // Инициализировать порты
    initPorts();

    // Инициализировать таймера
    initTimers();

    // Создать обьект 7 сегментного динамического дисплея, 4 цифры, общий анод
    SevSegDynamic display(SEG_A_PIN, SEG_B_PIN, SEG_C_PIN, SEG_D_PIN, SEG_E_PIN, SEG_F_PIN, SEG_G_PIN, SEG_DP_PIN,
                          DIG_1_PIN, DIG_2_PIN, DIG_3_PIN, DIG_4_PIN,
                          4, SevSegDynamic::COMMON_ANODE);

    // Создать обьект часов
    DS1302 rtc(DS1302_CE_PIN, DS1302_IO_PIN, DS1302_SCLK_PIN);

    // Создать обьект термодатчика
    DS18B20 termoSensor(DS18B20_1W_PIN);

    // Таймер 1 используется для подсчета количества импульсов со счетной турбины
    Timer_1 timerFlowCount(iTimer::PRESCALER_EXT_FALLING, iTimer::CTC);

    // В этот регистр позже будет записано калибровочное значение(количество импульсов), соответствующее 1 литру жидкости
    timerFlowCount.setCompareValue(0xFFFF);

    // Начинаем с нуля
    timerFlowCount.set(0);

    // Вызывать прерывание при сравнении, событие протекания 1 литра жидкости, если калибровка проведена
    timerFlowCount.setInterrupt(iTimer::COMPARE);

    // Создать обьект обработчика рабочего режима
    Worker worker(&display, &rtc, &termoSensor, &timerFlowCount);

    // Если нажата кнопка при старте - сброс всех счетчиков и настроек к заводским, а также часов
    if (!DigitalRead(BUTTON))
    {
        worker.factoryReset();
        worker.resetRTC();

        DigitalWrite(BUZER, Low);

        // Длинный звуковой сигнал
        for (uint8_t i = 0; i < 100; i++)
        {
            _delay_ms(10);
            wdt_reset();
        }

        DigitalWrite(BUZER, High);
    }

    // Создать обьеет меню
    Menu menu(&display, &rtc, &timerFlowCount);

    // Разрешить прерывания
    sei();

    uint8_t counterHalfMinute = 0;
    bool flag_30_Sec = true;
    uint8_t currendDeviceMode = Menu::modeWork;

    while (1)
    {
         // Сбросить вачдог таймер
         wdt_reset();

         // 200 раз в секунду обновлять дисплей
         if (flag_200_HZ)
         {
             if (currendDeviceMode == Menu::modeWork)
                worker.displayRefresh();
             else if (currendDeviceMode == Menu::modeMenu)
                menu.displayRefresh();

             flag_200_HZ = false;
         }

         // Если нажата кнопка как нибудь
         if (buttonShortPushed || buttonLongPushed)
         {
             // Если длительное нажатие в рабочем режиме
             if (buttonLongPushed && (currendDeviceMode == Menu::modeWork))
             {
                 // Установить режим работы меню
                 currendDeviceMode = Menu::modeMenu;

                 // Инициализировать меню
                 menu.init();
             }
             // Если в режиме меню
             else if (currendDeviceMode == Menu::modeMenu)
             {
                 // Вызвать обработчик конечного автомата меню
                 currendDeviceMode = menu.doFSM(buttonShortPushed, buttonLongPushed);

                 // Если выход из меню
                 if (currendDeviceMode == Menu::modeWork)
                 {
                     // Обновить время
                     worker.getRTC();

                     // Обновить счетчики, возможно какой то был сброшен
                     worker.updateFromNvRam();
                 }

                 // Для исключения дребезга кнопки
                 _delay_ms(10);
             }

             // Сбросить флаги нажатия кнопки
             buttonShortPushed = false;
             buttonLongPushed = false;
         }


         // Раз в секунду
         if (flag_1_Sec)
         {
             // Счетчики работают только в рабочем режиме
             if (currendDeviceMode == Menu::modeWork)
             {
                 // Обновить счетчики
                 worker.updateCounters();

                 // Следующее значение на дисплей
                 worker.displayNextValue();
             }

             if (counterHalfMinute++ > 30)
             {
                 counterHalfMinute = 0;
                 flag_30_Sec = true;
             }

             flag_1_Sec = false;
         }

         // Раз в полминуты
         if (flag_30_Sec)
         {
             // Получение температуры и времени только в рабочем режиме
             if (currendDeviceMode == Menu::modeWork)
             {
                 worker.getTemp();
                 worker.getRTC();
             }

             flag_30_Sec = false;
         }

         // Посчитан литр жидкости
         if (flag_flow_count)
         {
             if (currendDeviceMode == Menu::modeWork)
             {
                 worker.incAmounts();
             }

             flag_flow_count = false;
         }
    }
}

